#ifndef CALCGIZMO_H
#define CALCGIZMO_H
#include <vector>
#include <numeric>


/**
  * @file CalcGizmo.h
  * @version 0.2
  * @author Marian Stahl
  * @date  2018-05-16
  * @brief Helper functions for basic calculations
  */

/*!
 *  @addtogroup CalcGizmo
 *  @{
 */

//!Namespace for basic calculations
namespace CalcGizmo {

//!alias for SFINAE
template<typename F> using EnableIfFloatingPoint = typename std::enable_if<std::is_floating_point<typename std::decay<F>::type>::value, void>::type*;

/**
  * @fn FC1 asymmetry(FC1&& x, FC2&& y, EnableIfFloatingPoint<FC1> = nullptr, EnableIfFloatingPoint<FC2> = nullptr)
  * @brief calculate asymmetry
  * @param x
  * @param y
  * @return asymmetry with type deduced from x
  */
template <typename FC1, typename FC2>
inline FC1 asymmetry(FC1&& x, FC2&& y, EnableIfFloatingPoint<FC1> = nullptr, EnableIfFloatingPoint<FC2> = nullptr){
  return (x-y)/(x+y);
}

/**
  * @fn FC1 deff_asymmetry(FC1&& x, FC2&& y, FC3&& dx, FC4&& dy,
                             EnableIfFloatingPoint<FC1> = nullptr, EnableIfFloatingPoint<FC2> = nullptr,
                             EnableIfFloatingPoint<FC3> = nullptr, EnableIfFloatingPoint<FC4> = nullptr)
  * @brief calculate asymmetry uncertainty
  * @param x
  * @param y
  * @param dx : uncertainty of x
  * @param dy : uncertainty of y
  * @return asymmetry with type deduced from x
  */
template <typename FC1, typename FC2, typename FC3, typename FC4>
inline FC1 deff_asymmetry(FC1&& x, FC2&& y, FC3&& dx, FC4&& dy,
                          EnableIfFloatingPoint<FC1> = nullptr, EnableIfFloatingPoint<FC2> = nullptr,
                          EnableIfFloatingPoint<FC3> = nullptr, EnableIfFloatingPoint<FC4> = nullptr){
  return 2*std::sqrt((std::pow(x,2)*std::pow(dy,2)+std::pow(y,2)*std::pow(dx,2))/(std::pow((x+y),4)));
}

/**
  * @fn void pdgrounding(FC1&& x, FC2&& dx_stat, FC3&& dx_syst = 0.0,
                        EnableIfFloatingPoint<FC1> = nullptr, EnableIfFloatingPoint<FC2> = nullptr, EnableIfFloatingPoint<FC3> = nullptr)
  * @brief round values according to PDG standards
  * @param x : central value
  * @param dx_stat : statistical uncertainty
  * @param dx_syst : systematic uncertainty
  */
template <typename FC1, typename FC2, typename FC3>
inline void pdgrounding(FC1&& x, FC2&& dx_stat, FC3&& dx_syst = 0.0,
                        EnableIfFloatingPoint<FC1> = nullptr, EnableIfFloatingPoint<FC2> = nullptr, EnableIfFloatingPoint<FC3> = nullptr){
  if((dx_stat == 0 || dx_stat != dx_stat || dx_syst != dx_syst) && dx_syst == 0 )return;
  double uncertainty = std::max(dx_stat,static_cast<FC2>(dx_syst));
  int exponent = std::floor(std::log10(uncertainty));
  double significant_digit = std::pow(10,-exponent+2)*uncertainty;
  if(significant_digit < 354){
    x = (std::round(std::pow(10,-exponent+1)*x))*std::pow(10,exponent-1);
    dx_stat = (std::round(std::pow(10,-exponent+1)*dx_stat))*std::pow(10,exponent-1);
    dx_syst = (std::round(std::pow(10,-exponent+1)*dx_syst))*std::pow(10,exponent-1);
  }
  else{
    x = (std::round(std::pow(10,-exponent)*x))*std::pow(10,exponent);
    dx_stat = (std::round(std::pow(10,-exponent)*dx_stat))*std::pow(10,exponent);
    dx_syst = (std::round(std::pow(10,-exponent)*dx_syst))*std::pow(10,exponent);
  }
}

/**
  * @fn T sum(const std::vector<T>& xs)
  * @brief calculates sum of elements of a vector
  * @param xs : vector containing elements to sum up
  * @return sum of elements
  */
template <typename T>
inline T sum(const std::vector<T>& xs){
  return std::accumulate(xs.begin(),xs.end(),static_cast<T>(0));
}

/**
  * @fn T dsum(const std::vector<T>& dxs)
  * @brief calculate sqrt of sum of squares
  * @param dxs : vector containing elements to sum up
  * @return sqrt of sum of squared elements
  */
template <typename T>
inline T dsum(const std::vector<T>& dxs){
  return std::sqrt(std::accumulate(dxs.begin(),dxs.end(),static_cast<T>(0),[](const T& dxsq, const T& dx){return dxsq + std::pow(dx,2);}));
}

/**
  * @fn T mean(const std::vector<T>& xs)
  * @brief calculate mean
  * @param xs : vector to get mean from
  * @return mean of elements in input vector
  */
template <typename T>
inline T mean(const std::vector<T>& xs){
  return sum(xs)/static_cast<T>(xs.size());
}

/**
  * @fn T stdev(const std::vector<T>& xs)
  * @brief calculate standard deviation
  * @param xs : vector to get standard deviation from
  * @return standard deviation of elements in input vector
  * @details Unbiased estimator, see http://pdg.lbl.gov/2017/reviews/rpp2016-rev-statistics.pdf (page 3 eq. 39.6)
  */
template <typename T>
inline T stdev(const std::vector<T>& xs){
  const auto mu = mean(xs);
  return std::sqrt(std::accumulate(xs.begin(),xs.end(),static_cast<T>(0),[&mu](const T& xmmsq, const T& x){return xmmsq + std::pow(x-mu,2);})/static_cast<T>(xs.size()-1));
}

/**
  * @fn T dwmean(const std::vector<T> dxs)
  * @brief uncertainty on weighted mean
  * @param dxs : vector of uncertainties
  * @return uncertainty on weighted mean
  * @details See http://pdg.lbl.gov/2017/reviews/rpp2016-rev-statistics.pdf (page 4 eq. 39.8 etc.)
  */
template <typename T>
inline T dwmean(const std::vector<T> dxs){
  return 1/std::sqrt(std::accumulate(dxs.begin(),dxs.end(),static_cast<T>(0),
                                     [](const T& w, const T& dx){return dx > 0 ? w + 1/std::pow(dx,2) : 0;}));
}

/**
  * @fn T1 wmean(const std::vector<T1>& xs, const std::vector<T2> dxs)
  * @brief calculate weighted mean (weights based on uncertainties)
  * @param xs : vector of central values
  * @param dxs : vector of uncertainties
  * @return weighted mean of elements in input vector
  * @details See http://pdg.lbl.gov/2017/reviews/rpp2016-rev-statistics.pdf (page 4 eq. 39.8)
  */
template <typename T1, typename T2>
inline T1 wmean(const std::vector<T1>& xs, const std::vector<T2> dxs){
  //is there a nice STL way to use accumulate with multiple *const* input vectors?
  //in python there's zip, with boost, there are variadic template solutions, but this would be canons & birds
  T1 x_bar = 0;
  for(decltype(xs.size()) i = 0; i < xs.size(); i++)
    x_bar += dxs[i] > 0 ? xs[i]/std::pow(dxs[i],2) : static_cast<T1>(0);
  return x_bar*std::pow(dwmean(dxs),2);
}

/**
  * @fn FC1 e1cm_2body(FC1&& M, FC2&& m1, FC3&& m2, EnableIfFloatingPoint<FC1> = nullptr, EnableIfFloatingPoint<FC2> = nullptr, EnableIfFloatingPoint<FC3> = nullptr)
  * @brief calculate center of mass energy of the first decay product of a 2-body decay 0 -> 1 2 with mass(0) = M, mass(1) = m1, mass(2) = m2
  * @param M: mass of decaying particle
  * @param m1: mass of first decay product
  * @param m2: mass of second decay product
  * @return energy of first decay product with type deduced from M
  */
template <typename FC1=float, typename FC2=float, typename FC3=float>
inline typename std::decay<FC1>::type ecm_2body(FC1&& M, FC2&& m1, FC3&& m2,
                                                EnableIfFloatingPoint<FC1> = nullptr, EnableIfFloatingPoint<FC2> = nullptr, EnableIfFloatingPoint<FC3> = nullptr){
  return (std::pow(M,2)-std::pow(m2,2)+std::pow(m1,2))/(2*M);
}

/**
  * @fn FC1 pcm_2body(FC1&& M, FC2&& m1, FC3&& m2, EnableIfFloatingPoint<FC1> = nullptr, EnableIfFloatingPoint<FC2> = nullptr, EnableIfFloatingPoint<FC3> = nullptr)
  * @brief calculate absolute center of mass momentum of a 2-body decay 0 -> 1 2 with mass(0) = M, mass(1) = m1, mass(2) = m2
  * @param M: mass of decaying particle
  * @param m1: mass of first decay product
  * @param m2: mass of second decay product
  * @return momentum with type deduced from M
  */
template <typename FC1=float, typename FC2=float, typename FC3=float>
inline typename std::decay<FC1>::type pcm_2body(FC1&& M, FC2&& m1, FC3&& m2,
                                                EnableIfFloatingPoint<FC1> = nullptr, EnableIfFloatingPoint<FC2> = nullptr, EnableIfFloatingPoint<FC3> = nullptr){
  return std::sqrt((std::pow(M,2)-std::pow(m1+m2,2))*(std::pow(M,2)-std::pow(m1-m2,2)))/(2*M);
}

/**
  * @fn FC1 pr_endpoint(FC1&& M, FC2&& m12, FC3&& m1, FC4&& m2, FC5&& m3, bool min, EnableIfFloatingPoint<FC1> = nullptr, EnableIfFloatingPoint<FC2> = nullptr, EnableIfFloatingPoint<FC3> = nullptr, EnableIfFloatingPoint<FC4> = nullptr, EnableIfFloatingPoint<FC5> = nullptr)
  * @brief calculate kinematic endpoints of partially reconstructed backgrounds
  * @param M: mass of decaying particle
  * @param m12: mass of quasi-stable resonance
  * @param m1: mass of the particle that has not been reconstructed
  * @param m2: mass of first reconstructed decay product
  * @param m3: mass of second reconstructed decay product
  * @param max: calculate upper (lower) endpoint if true (false); true by default
  * @return invariant mass m23 of kinematic endpoint with type deduced from M
  * @details Consider the three body decay 0 -> (12) 3, where (12) form a quasi-stable resonance and particle 1 is not reconstructed.
  * The kinematic endpoints of that resonance in the invariant mass system of 23 is fully determined by the masses involved in the decay:
  * M, m12, m1, m2, m3 - the masses of particle 0, (12), 1, 2 and 3 respectively. <br>
  * An example would be $\Lambda_b^0\rightarrow\Lambda_c^+ [D_s^-\gamma]_{D_s^{\ast-}}$ where the photon has not been reconstructed. <br>
  * See http://pdg.lbl.gov/2017/reviews/rpp2017-rev-kinematics.pdf sec. 48.4.3.1. for all the details.
  */
template <typename FC1=float, typename FC2=float, typename FC3=float, typename FC4=float, typename FC5=float>
inline typename std::decay<FC1>::type pr_endpoint(FC1&& M, FC2&& m12, FC3&& m1, FC4&& m2, FC5&& m3, bool max=true,
        EnableIfFloatingPoint<FC1> = nullptr, EnableIfFloatingPoint<FC2> = nullptr, EnableIfFloatingPoint<FC3> = nullptr,
        EnableIfFloatingPoint<FC4> = nullptr, EnableIfFloatingPoint<FC5> = nullptr){
          auto const E2Star = ecm_2body(std::forward<FC2>(m12),std::forward<FC4>(m2),std::forward<FC3>(m1)); //energy of 2 in the 12 RF
          auto const E3Star = (std::pow(M,2)-std::pow(m12,2)-std::pow(m3,2))/(2*m12);//energy of 3 in the 12 RF
  return std::sqrt(std::pow(E2Star+E3Star,2)-std::pow(std::sqrt(std::pow(E2Star,2)-std::pow(m2,2))+std::pow(-1.,max)*std::sqrt(std::pow(E3Star,2)-std::pow(m3,2)),2));
}
}
/*! @} End of Doxygen Groups*/
//TODO: linear least sqares including correlations
#endif
