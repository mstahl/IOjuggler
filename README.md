# Contributing and Reusing
This is an open analysis tool. Please have a look at our [guidelines](CONTRIBUTING.md) for contributions. If you want to reuse this code in your own project please note the [reciprocity](RECIPROCITY.md) statement.

# Purpose
IOjuggler is a header only library that handles parsing of options, and facilitates the interaction with ROOT objects.
Further scripts for debugging purposes, basic calculations, a message service and a progress bar are included as well.
There is a doxygen documentation [here](https://www.physi.uni-heidelberg.de/~mstahl/IOjuggler/html/).<br>

The scripts are briefly described in the following.

## CalcGizmo
CalcGizmo is used for small basic calculations.
The script defines an own namespace. Use like
```c++
#include <CalcGizmo.h>
...
auto mysum = CalcGizmo::sum(my_std_vector_of_summable_objects);
```
The individual functions are described [here](https://www.physi.uni-heidelberg.de/~mstahl/IOjuggler/html/CalcGizmo_8h.html).

## debug_helpers
Provides functions for debugging and helpful printouts for fitting with RooFit, documented [here](https://www.physi.uni-heidelberg.de/~mstahl/IOjuggler/html/debug_helpers_8h.html).

## IOjuggler
The main script to parse command line options and print or manipulate boost property trees. [Doxy](https://www.physi.uni-heidelberg.de/~mstahl/IOjuggler/html/IOjuggler_8h.html)

## MessageService
A small class providing a common printout style to stdout. It has four verbosity settings: `DEBUG`, `INFO`, `WARNING` and `ERROR`. The calling script can use it like:
```c++
MessageService msgsvc("createWorkspace",static_cast<MSG_LVL>(options.get<int>("verbosity")));
msgsvc.debugmsg("Some DEBUG printout");
msgsvc.infomsg("Some INFO printout");
msgsvc.warningmsg("Some WARNING printout");
msgsvc.errormsg("Some ERROR printout");
```
In the first line the service is initialised, parsing the verbosity level from the command line with the help of `parse_options` from `IOjuggler.h`.
If that level is for example WARNING ('-v1'), only the warning and error-printout are printed in stdout.<br>
The class is documented [here](https://www.physi.uni-heidelberg.de/~mstahl/IOjuggler/html/classMessageService.html).

## ProgressBar
Prints an ASCII progress bar to stdout. To be used in loops which increment a number and where each step takes roughly the same time.
Documented [here](https://www.physi.uni-heidelberg.de/~mstahl/IOjuggler/html/classProgressBar.html)

## RootGetter
Some basic functions including safeguards to get a ROOT file or stuff from anywhere in a ROOT file or `RooWorkspace`.
The script also includes a function that checks if a directory exists on the filesystem and might create it.
Documented [here](https://www.physi.uni-heidelberg.de/~mstahl/IOjuggler/html/RootGetter_8h.html)
