#ifndef MESSAGESERVICE_H
#define MESSAGESERVICE_H

#include "TString.h"
#include <iostream>

#ifndef IOJ_GIT_HASH
#define IOJ_GIT_HASH " "
#endif

/**
  * @file MessageService.h
  * @version 0.1
  * @author Marian Stahl
  * @date  2018-05-07
  * @brief The file contains a small class for printing strings which are convertible to TString to stdout
  */

//!alias for SFINAE: checks if template type is convertible to TString
template<typename S> using EnableIfstringConvertible = typename std::enable_if<std::is_convertible<S,TString>::value, void>::type*;

/**
* @enum MSG_LVL
* @brief Gives meaningful names to message levels
*/
enum class MSG_LVL{ERROR = 0, WARNING, INFO, DEBUG};

/**
  * @class MessageService
  * @version 0.2
  * @author Marian Stahl
  * @date 2018-05-16
  * @brief Helper class for nice stdout printing
  * @details Easily usable interface to print messages to stdout, depending on the level of verbosity.
  */
class MessageService{
  friend class ProgressBar;
public:
  /**
    * @fn MessageService()
    * @brief Constructor
    * @details Sets program name which is printed in a message to an empty string and message level to INFO
    */
  MessageService(){
    pname = "";
    message_level = MSG_LVL::INFO;
  }

  /**
    * @fn MessageService(STR&& name, MSG_LVL verbosity = MSG_LVL::INFO, EnableIfstringConvertible<STR> = nullptr)
    * @brief Constructor with calling program name and message level
    * @param name: Name of the calling program
    * @param verbosity: Highest message level that should be printed to stdout
    */
  template <typename STR = TString>
  MessageService(STR&& name, MSG_LVL verbosity = MSG_LVL::INFO, EnableIfstringConvertible<STR> = nullptr)
    : pname{ static_cast<TString>(name) }, message_level{ verbosity }{
    this->debugmsg("Current IOjuggler git hash: " + static_cast<std::string>(IOJ_GIT_HASH));
  }

  /**
  * @fn inline void debugmsg(STR&& input)
  * @param input: Debug message to be printed
  * @brief Prints a debug message
  * @details Debug messages are suppressed if \c message_level is smaller than DEBUG (3)
  */
  template <typename STR = TString>
  inline void debugmsg(STR&& input, EnableIfstringConvertible<STR> = nullptr) const {
    if(message_level >= MSG_LVL::DEBUG)
      std::printf("\033[0;36m%-32.32s \033[1;32m%-8.8s\033[0m %s\n",pname.Data(),"DEBUG:",static_cast<TString>(input).Data());
  }

  /**
  * @fn inline void infomsg(STR&& input)
  * @param input: Info message to be printed
  * @brief Prints a info message
  * @details Info messages are suppressed if \c message_level is smaller than INFO (2)
  */
  template <typename STR = TString>
  inline void infomsg(STR&& input, EnableIfstringConvertible<STR> = nullptr) const {
    if(message_level >= MSG_LVL::INFO)
      std::printf("\033[0;36m%-32.32s \033[1;37m%-8.8s\033[0m %s\n",pname.Data(),"INFO:",static_cast<TString>(input).Data());
  }

  /**
  * @fn inline void warningmsg(STR&& input)
  * @param input: Warning message to be printed
  * @brief Prints a warning message
  * @details Warning messages are suppressed if \c message_level is smaller than WARNING (1)
  */
  template <typename STR = TString>
  inline void warningmsg(STR&& input, EnableIfstringConvertible<STR> = nullptr) const {
    if(message_level >= MSG_LVL::WARNING)
      std::printf("\033[0;36m%-32.32s \033[1;33m%-8.8s\033[0m %s\n",pname.Data(),"WARNING:",static_cast<TString>(input).Data());
  }

  /**
  * @fn inline void errormsg(STR&& input)
  * @param input: Error message to be printed
  * @brief Prints a error message
  * @details Error messages are never suppressed
  */
  template <typename STR = TString>
  inline void errormsg(STR&& input, EnableIfstringConvertible<STR> = nullptr) const {
    if(message_level >= MSG_LVL::ERROR)
      std::printf("\033[0;36m%-32.32s \033[1;31m%-8.8s\033[0m %s\n",pname.Data(),"ERROR:",static_cast<TString>(input).Data());
  }

  /**
  * @fn inline void SetMsgLvl(int lvl)
  * @param lvl: Highest message level as \c int that should be printed to stdout
  * @brief Sets message level
  */
  inline void SetMsgLvl(int lvl){
    if(lvl < 0 || lvl > 3) warningmsg("Message level should be between 0 and 3. This may result in unwanted behaviour!");
    message_level = static_cast<MSG_LVL>(lvl);
  }

  /**
  * @fn inline void SetMsgLvl(MSG_LVL lvl)
  * @param lvl: Highest message level as \c MSG_LVL that should be printed to stdout
  * @brief Sets message level
  */
  inline void SetMsgLvl(MSG_LVL lvl){
    message_level = lvl;
  }

  /**
  * @fn inline MSG_LVL GetMsgLvl()
  * @brief Gets message level
  */
  inline MSG_LVL GetMsgLvl() const {
    return message_level;
  }

  /**
  * @fn inline void AppendPname(STR&& name_to_append)
  * @param name_to_append: string type which is castable to TString
  * @brief Appends this name to the name of the current instance of MessageService
  */
  template <typename STR = TString>
  inline void AppendPname(STR&& name_to_append, EnableIfstringConvertible<STR> = nullptr){
    pname += static_cast<TString>(name_to_append);
  }

  MessageService(const MessageService &) = default;
  MessageService(MessageService &&) = default;

  ~MessageService() = default;

  MessageService &operator=(const MessageService &) = default;
  MessageService &operator=(MessageService &&) = default;

private:
  //!Name of the calling programm (or whatever is passed to the c-tor)
  TString pname;
  //!Verbosity level
  MSG_LVL message_level;
  /**
  * @fn inline void print_progress(unsigned int percent, double ETA, int nthreads) const
  * @param percent: Percentage of work done
  * @param ETA: Estimated time
  * @param nthreads: number of threads
  * @brief Prints status bar
  */
  inline void print_progress(unsigned int percent, double ETA, int nthreads) const {
    if(message_level >= MSG_LVL::INFO){
      const auto bar = std::string(percent/2, '=');
      const auto spaces = std::string(50-percent/2, ' ');
      const std::string threads = nthreads > 1 ? "threads" : "thread";
      std::printf("\r\033[2K\033[0;36m%-32.32s \033[1;37m%-8.8s\033[0m[%s%s] (%u%% - ETA: %.0fs - %d %s)",
                  pname.Data(),"INFO:",bar.data(),spaces.data(),percent,ETA,nthreads,threads.data());
      fflush(stdout);
    }

  }

};
#endif
