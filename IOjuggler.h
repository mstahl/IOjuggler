#ifndef IOJUGGLER_H
#define IOJUGGLER_H
//ROOT
#include "TError.h"
#include "RooMsgService.h"
#include "TFriendElement.h"
#include "TChain.h"
#include "TObjString.h"
#include "Compression.h"
//C++
#include <algorithm>
#include <array>
#include <cstdlib>
#include <getopt.h>
#include <iostream>
#include <type_traits>
#include <typeinfo>
#include <utility>
#include <vector>
//BOOST
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/optional.hpp>
//local
#include "RootGetter.h"

/**
  * @file IOjuggler.h
  * @version 0.9
  * @author Marian Stahl
  * @date 2019-03-10
  * @brief Helper functions for parsing command line arguments and for property tree gymnastics
  */

namespace pt = boost::property_tree;

extern char* optarg;
extern int optind;

/*!
 *  @addtogroup IOjuggler
 *  @{
 */

/*! @brief Namespace for functions defined in RootGetter.h and IOjuggler.h
 *  @details The functions live in different files because RootGetter.h does not depend on the boost libraries and can therefore be used with the ROOT built-in compiler<br>
 *   The namespace is intended to make the code in the calling script more readable
 */
namespace IOjuggler {

  /**
    * @fn void dir_exists(TString cpn, const MessageService& msgsvc = MessageService("IOjuggler:dir_exists",MSG_LVL::INFO), const bool& create = true)
    * @param dofn: directory- or file-name
    * @param msgsvc: to use external MessageService and its settings
    * @param create: create directory if it doesn't exist
    * @brief Check if a directory exists given a directoy or filename as input
    */
  template <typename STR = TString>
  inline void dir_exists(STR&& dofn, const MessageService& msgsvc = MessageService("RootGetter:dir_exists",MSG_LVL::INFO),
                         const bool& create = true, EnableIfstringConvertible<STR> = nullptr){
    auto const path_to_check = fs::path(static_cast<std::string>(dofn)).remove_filename().make_preferred();
    if(fs::exists(path_to_check) == false && create && !path_to_check.empty()){
      msgsvc.infomsg("Creating new directory: " + path_to_check.string());
      auto const created_directories = fs::create_directories(path_to_check);
      if(created_directories == false){
        auto const status = gSystem->mkdir(path_to_check.c_str(),true);
        if(status == -1)
          msgsvc.errormsg("Directory "+path_to_check.string()+" can not be created. Please check path");
      }
    }
  }

/**
  * @fn void print_ptree(const pt::ptree& configtree, const MessageService& msgsvc = MessageService("IOjuggler:print_ptree",MSG_LVL::INFO), std::string&& indentation = std::string())
  * @param configtree: ptree/child node that is printed
  * @param msgsvc : external or dummy MessageService
  * @param indentation : used for recursive calls only
  * @brief Prints a property tree with indentation
  * @details Prints a property tree to stdout. Mainly for debugging purposes
  */
inline void print_ptree(const pt::ptree& configtree, const MessageService& msgsvc = MessageService("IOjuggler:print_ptree",MSG_LVL::INFO), std::string&& indentation = std::string()){
  //iterate childs
  for (const auto& it : configtree) {
    if(it.second.data().empty()){//a child node
      msgsvc.infomsg(indentation + it.first + " {");
      indentation += "  ";
    }
    else{//if the current key value pair is not a child node, print it (even if the value is empty)
      if(it.first.size() <= 20)
        msgsvc.infomsg(TString::Format("%s%-20.20s %s",indentation.data(),it.first.data(),it.second.get_value<std::string>().data()));
      else
        msgsvc.infomsg(TString::Format("%s%-17.17s... %s",indentation.data(),it.first.data(),it.second.get_value<std::string>().data()));
    }
    //ptree inception... go to the next deeper level
    print_ptree(it.second,msgsvc,std::move(indentation));
    //close brackets again and remove indentations
    if(it.second.data().empty()){
      if(!indentation.empty()){
        indentation.pop_back();indentation.pop_back();
      }
      msgsvc.infomsg(indentation + "}");
    }
  }
}

/**
  * @fn const pt::ptree parse_options(int argc, char** argv, STRING&& available_options, std::string extraopts_help = "",
                              int extraopts_mandatory = 0, EnableIfstringConvertible<STRING> = nullptr)
  * @param argc: argc option to calling executable
  * @param argv: argv option to calling executable
  * @param available_options: Options that should be parseable from the executable. The maximally parseable options are currently "c:d:hi:m:o:p:r:t:v:w:" + nonoptions.
  * h doesn't require an argument, thus no colon behind it. See http://man7.org/linux/man-pages/man3/getopt.3.html
  * @param extraopts_help: help string to be printed when extra options are used
  * @param extraopts_mandatory: number of mandatory extra options, throws error if less are passed
  * @details Run the executable that calls this function with \code my_executable -h \endcode to see the available options
  * @note using SFINAE method for template argument, which requires convertibility to TString (true for at least all value types of std::string, char, TString)
  * @return a ptree with the parsed options as value type
  */
template <typename STRING>
const pt::ptree parse_options(int argc, char** argv, STRING&& available_options, std::string extraopts_help = "",
                              int extraopts_mandatory = 0, EnableIfstringConvertible<STRING> = nullptr){

  pt::ptree parsed_options;

  TString opts = static_cast<TString>(available_options);
  char* options = const_cast<char*>(opts.Data());

  MessageService msgsvc("IOjuggler:parse_options",MSG_LVL::INFO);

  TString command = static_cast<TString>(argv[0]);
  for (int i = 1; i < argc; i++)
    command += " " + static_cast<TString>(argv[i]);
  msgsvc.infomsg(command);

  auto printhelp = [&argv, &opts, &extraopts_help, &msgsvc] (){
    msgsvc.infomsg("Usage:");
    TString message = static_cast<TString>(argv[0]);
    if(opts.Contains("b")) message += " -b <background inputfile(s)>";
    if(opts.Contains("c")) message += " -c <configfile>";
    if(opts.Contains("i")) message += " -i <inputfile(s)>";
    if(opts.Contains("s")) message += " -s <signal inputfile(s)>";
    if(opts.Contains("t")) message += " -t <treename>";
    if(opts.Contains("o")) message += " -o <outputfile>";
    if(opts.Contains("f")) message += " -f <MVA factory name>";
    if(opts.Contains("p")) message += " -p <prefix wildcard>";
    msgsvc.infomsg(message);
    if(opts.Contains("d") || opts.Contains("j") || opts.Contains("m") || opts.Contains("w") || opts.Contains("v")) msgsvc.infomsg("Options:");
    if(opts.Contains("d")){msgsvc.infomsg("-d <workdir (default=\"\")>  NOTE: All outputs will be prepended with <workdir>");}
    if(opts.Contains("j")) msgsvc.infomsg("-j a \"joker\" string. Mostly to do command line manipulation of the input config file with this pattern:"\
                                          " <search_for0:replace_by0;search_for1:replace_by1;... (default=\"\")> Allows to search and replace strings in config file at runtime");
    if(opts.Contains("m")) msgsvc.infomsg("-m <MVA weight file (default=\"\")>");
    if(opts.Contains("n")) msgsvc.infomsg("-n <a number (default=1000u)>");
    if(opts.Contains("r")) msgsvc.infomsg("-r <name of RooDataSet (default=\"ds\")>");
    if(opts.Contains("v")) msgsvc.infomsg("-v <verbosity level (default=2, 0-3, higher: more verbose)>");
    if(opts.Contains("w")) msgsvc.infomsg("-w <workspace name (default=\"w\")>");
    if(!extraopts_help.empty()) msgsvc.infomsg(extraopts_help);
    return;
  };

  int ca;
  while ((ca = getopt(argc, argv, options)) != -1){
    switch (ca) {
      case 'h':
        printhelp();
        std::exit(EXIT_SUCCESS);
      case 'b':
        parsed_options.put("bkg_inputs",std::string(optarg));
        break;
      case 'c':
        parsed_options.put("config",std::string(optarg));
        break;
      case 'd':
        parsed_options.put("workdir",std::string(optarg));
        break;
      case 'f':
        parsed_options.put("factoryname",std::string(optarg));
        break;
      case 'i':
        parsed_options.put("infilename",std::string(optarg));
        break;
      case 'j':
        parsed_options.put("joker",std::string(optarg));
        break;
      case 'm':
        parsed_options.put("weightfile",std::string(optarg));
        break;
      case 'n':
        parsed_options.put("n",std::string(optarg));
        break;
      case 'p':
        parsed_options.put("prefix",std::string(optarg));
        break;
      case 'o':
        parsed_options.put("outfilename",std::string(optarg));
        break;
      case 'r':
        parsed_options.put("dsname",std::string(optarg));
        break;
      case 's':
        parsed_options.put("sig_inputs",std::string(optarg));
        break;
      case 't':
        parsed_options.put("treename",std::string(optarg));
        break;
      case 'v':
        parsed_options.put("verbosity",atoi(optarg));
        msgsvc.SetMsgLvl(atoi(optarg));
        break;
      case 'w':
        parsed_options.put("wsname",std::string(optarg));
        break;
      case '?':
        msgsvc.warningmsg(TString::Format("Unknown option: %s",optarg));
        break;
      default:
        msgsvc.warningmsg(TString::Format("?? getopt returned character code 0%o ??",ca));
    }
  }


  // put list of arguments which can't be parsed into the property tree
  //if (optind >= argc) {
  // if we reqire additional paremters give an error message
  //}
  pt::ptree extraargtree;
  int extraopts_num = 0;
  while (optind < argc) {
    // use a little trick to create a list of values
    extraopts_num++;
    pt::ptree ptr1;
    ptr1.put("",std::string(argv[optind++]));
    extraargtree.push_back(std::make_pair("", ptr1));
  }
  parsed_options.add_child("extraargs",extraargtree);

  //now check mandatory options
  if((opts.Contains("b") && !parsed_options.get_optional<std::string>("bkg_inputs"))
     || (opts.Contains("c") && !parsed_options.get_optional<std::string>("config"))
     || (opts.Contains("i") && !parsed_options.get_optional<std::string>("infilename"))
     || (opts.Contains("o") && !parsed_options.get_optional<std::string>("outfilename"))
     || (opts.Contains("s") && !parsed_options.get_optional<std::string>("sig_inputs"))
     || (opts.Contains("t") && !parsed_options.get_optional<std::string>("treename"))
     || (opts.Contains("f") && !parsed_options.get_optional<std::string>("factoryname"))
     || (extraopts_num < extraopts_mandatory)){
    msgsvc.errormsg("A mandatory option has not been passed, please reconfigure available options or pass the required options to the executable!");
    printhelp();
    throw std::runtime_error("IOjuggler: A mandatory option has not been passed to parse_options function");
  }

  //set defaults in case optional options are available
  if( opts.Contains("d") && !parsed_options.get_optional<std::string>("workdir") )
    parsed_options.put("workdir",std::string(""));
  else if(opts.Contains("d"))//check if given workdir exists. if not, create it! -> delegate to dir_exists in RootGetter.h
    dir_exists(parsed_options.get<std::string>("workdir"));
  if( opts.Contains("w") && !parsed_options.get_optional<std::string>("wsname") )
    parsed_options.put("wsname",std::string("w"));
  if( opts.Contains("r") && !parsed_options.get_optional<std::string>("dsname") )
    parsed_options.put("dsname",std::string("ds"));
  if( opts.Contains("j") && !parsed_options.get_optional<std::string>("joker") )
    parsed_options.put("joker",std::string(""));
  if( opts.Contains("m") && !parsed_options.get_optional<std::string>("weightfile") )
    parsed_options.put("weightfile",std::string(""));
  if( opts.Contains("n") && !parsed_options.get_optional<std::string>("n") )
    parsed_options.put("n",1000u);
  if( opts.Contains("v") && !parsed_options.get_optional<std::string>("verbosity") )
    parsed_options.put("verbosity",2);

  //print all settings
  if(opts.Contains("v") && parsed_options.get<int>("verbosity") > 1)
    print_ptree(parsed_options,msgsvc);

  return parsed_options;//the ptree is moved implicitly!
}

/**
  * @fn pt::ptree get_ptree(STR&& config, bool print = false, bool ignore_empty = false, EnableIfstringConvertible<STR> = nullptr)
  * @param config: location of config file
  * @param print: print the read-in ptree
  * @param ignore_empty: ignores empty ptrees (use this flag to, for exmaple, check if temporary files exist)
  * @return pt::ptree
  */
template <typename STR = TString>
inline pt::ptree get_ptree(STR&& config, bool print = false, bool ignore_empty = false, EnableIfstringConvertible<STR> = nullptr){
  pt::ptree configtree;
  //configtree will be set to an empty tree if not found
  pt::read_info(static_cast<TString>(config).Data(),configtree,configtree);
  if(configtree.empty() && !ignore_empty){
    MessageService msgsvc("IOjuggler:get_ptree",MSG_LVL::INFO);
    msgsvc.errormsg("The config file could not be parsed. Please carefully check all parantheses and quotation marks.");
  }
  if(print)print_ptree(configtree);
  return configtree;
}

/**
  * @fn void replace_stuff_in_ptree(pt::ptree& conf, const std::string& replace, const std::string& by, const std::string& own_node = "REPLACE",
  *   const std::vector<std::string>& ignore_nodes = {"APPEND"})
  * @param conf: config file/child node in which stuff gets replaced
  * @param replace: string to replace
  * @param by: string to insert
  * @param own_node: name of the node containing the \c replace \c by pair (so \c replace isn't replaced)
  * @param ignore_nodes: ptree children which are ignored by this function
  * @brief iterates a ptree and replaces \c replace by \c by in all ancestor nodes
  */
void replace_stuff_in_ptree(pt::ptree& conf, const std::string& replace, const std::string& by, const std::string& own_node = "REPLACE",
                            const std::vector<std::string>& ignore_nodes = {"APPEND"}){
  //we need a copy of ignore_nodes in this scope to avoid circular replacements ("by"-string contains "replace" string)
  std::vector<std::string> ign_keys;
  for (auto& it : conf) {
    //ignore own_node or ignore_nodes
    if(it.first == own_node || std::find_if(ignore_nodes.begin(),ignore_nodes.end(),[&it](const auto& ign){return it.first == ign;}) != ignore_nodes.end())
      continue;
    if(it.second.get_value<std::string>().find(replace) != std::string::npos){
      auto value = it.second.get_value<std::string>();
      boost::algorithm::replace_all(value,replace,by);
      conf.put(it.first,value);//this should be in-place - no need for "protection" like below
    }
    const bool already_replaced_this_key = std::find_if(ign_keys.begin(),ign_keys.end(),[&it](const auto& ign){return it.first == ign;}) != ign_keys.end();
    if(!already_replaced_this_key && it.first.find(replace) != std::string::npos){
      auto key = it.first;
      // "replacement-loop" triggered by a "by" string that looks like this [a,b,c].
      if(by.find("[",0) == 0){
        auto substi = by.substr(1,by.size()-2); // make a copy that we can modify
        boost::algorithm::erase_all(substi," ");
        std::vector<std::string> parsed_substitutes;
        boost::algorithm::split(parsed_substitutes, substi, boost::algorithm::is_any_of(","));
        // we need to make a copy of the node containing the "replace" string
        auto original_node = conf.get_child(key);
        for(const auto& sub : parsed_substitutes){
          if(sub.empty()) continue;
          auto copykey = key; // copy the original node-name
          boost::algorithm::replace_all(copykey,replace,sub); // do substitution in that node-name
          if(it.second.data().empty()){ // it's a node
            conf.push_back({copykey,original_node}); // add a node with the new name to the back of the config. use the content of the node as it was
            replace_stuff_in_ptree(conf.get_child(copykey),replace,sub,own_node,ignore_nodes); // make subsitutions within the new node
          }
          else // not node, but key value pair
            conf.put(copykey,boost::algorithm::replace_all_copy(it.second.data(),by,sub));
        }
      }// do a normal key replacement if there's no special keyword
      else {
        boost::algorithm::replace_all(key,replace,by);
        //prefer push_back over add_child or put_child because we don't want to create substructures here (by a full stop '.')
        conf.push_back({key,conf.get_child(it.first)});//this adds a new child node at the back of the conf containter (so we will run on it later again)
      }
      conf.erase(it.first);
      ign_keys.push_back(std::move(key));//don't run in circles
      continue;
    }
    //maybe there are nodes downstream which need to be replaced, so don't pass ign_keys, but ignore_nodes
    replace_stuff_in_ptree(it.second,replace,by,own_node,ignore_nodes);
  }
}

/**
  * @fn void auto_replace_in_ptree(pt::ptree& conf, const std::string& own_node = "REPLACE", const std::vector<std::string>& ignore_nodes = {"APPEND"})
  * @param conf: config file/child node in which stuff gets replaced
  * @param own_node: name of the node containing key value pairs indicating what gets replaced by what
  * @param ignore_nodes: ptree children which are ignored by this function
  * @brief iterates a ptree, searches for \c own_node (default: "REPLACE") and iteratively replaces it's keys by values recursively in the parent ptree
  * @details consider a ptree like:
  *   \code
  *   mynode0 {
  *     key_in_0 stuff
  *     REPLACE {
  *       stuff something_useful
  *     }
  *     foo {
  *       stuff value_in_foo
  *     }
  *     bar {
  *       key_in_bar stuff
  *     }
  *   }
  *   mynode1 {
  *     key_in_1 stuff
  *     REPLACE {
  *       stuff something_else
  *     }
  *   }
  *   \endcode
  * In \c mynode0 , auto_replace_in_ptree will replace all appearences of \c stuff (key or value doesn't matter) by \c something_useful (except in the \c REPLACE node of course).
  * In mynode1, it will replace \c stuff by \c something_else
  */
void auto_replace_in_ptree(pt::ptree& conf, const std::string& own_node = "REPLACE", const std::vector<std::string>& ignore_nodes = {"APPEND"}){
  for (auto& it : conf) {//iterate through all direct children
    if(it.first == own_node)//replace-node found
      for(const auto& rep : conf.get_child(own_node))//iterate it!
        replace_stuff_in_ptree(conf,rep.first,rep.second.get_value<std::string>(),own_node,ignore_nodes);//call the recursive replace function
    auto_replace_in_ptree(it.second,own_node,ignore_nodes);//go down one generation
  }
}

/**
  * @fn void append_stuff_in_ptree(pt::ptree& conf, const std::string& key, const std::string& append,
  *                           const std::string& own_node = "APPEND", const std::vector<std::string>& ignore_nodes = {"REPLACE"})
  * @param conf: config file/child node in which stuff gets appended
  * @param key: name of key to whose value \c append is appended to
  * @param append: string to append
  * @param own_node: name of the node containing the \c key \c append pair
  * @param ignore_nodes: ptree children which are ignored by this function
  * @todo Maybe also appending something to a key would be useful...
  */
void append_stuff_in_ptree(pt::ptree& conf, const std::string& key, const std::string& append,
                           const std::string& own_node = "APPEND", const std::vector<std::string>& ignore_nodes = {"REPLACE"}){
  for (auto& it : conf) {
    //ignore own_node or ignore_nodes
    if(it.first == own_node || std::find_if(ignore_nodes.begin(),ignore_nodes.end(),[&it](const auto& ign){return it.first == ign;}) != ignore_nodes.end())
      continue;
    if(it.first.find(key) != std::string::npos){
      conf.put(it.first,it.second.get_value<std::string>() + append);
      continue;
    }
    append_stuff_in_ptree(it.second,key,append,own_node,ignore_nodes);
  }
}

/**
  * @fn void auto_append_in_ptree(pt::ptree& conf, const std::string& own_node = "APPEND", const std::vector<std::string>& ignore_nodes = {"REPLACE"})
  * @param conf: config file/child node in which stuff gets appended
  * @param own_node: name of the node containing the key to search for and the value to append
  * @param ignore_nodes: ptree children which are ignored by this function
  * @brief Iterates a ptree, searches for \c own_node (default: "APPEND") and iteratively searches for it's keys recursively in the parent ptree.
  * When such a key was found, the value in \c own_node is appended to the value corresponding to the found key.
  * @details consider a ptree like:
  *   \code
  *   mynode0 {
  *     some_key stuff
  *     APPEND {
  *       some_key more_stuff
  *     }
  *     foo {
  *       foo_key some_key
  *     }
  *     bar {
  *       some_key stuff_bar
  *     }
  *   }
  *   mynode1 {
  *     key_in_1 stuff
  *     APPEND {
  *       key_in_1 something_else
  *     }
  *   }
  *   \endcode
  * In \c mynode0 , auto_append_in_ptree will append \c more_stuff to \c stuff and \c more_stuff to \c stuff_bar in \c bar because the keys of the \c APPEND node matches their keys.
  * It does however NOT append \c more_stuff to \c some_key in \c foo, because \c some_key is a value in \c foo.
  * In mynode1, it will append \c something_else to \c stuff.
  */
void auto_append_in_ptree(pt::ptree& conf, const std::string& own_node = "APPEND", const std::vector<std::string>& ignore_nodes = {"REPLACE"}){
  for (auto& it : conf) {//iterate through all direct children
    if(it.first == own_node)//append-node found
      for(const auto& rep : conf.get_child(own_node))//iterate it!
        append_stuff_in_ptree(conf,rep.first,rep.second.get_value<std::string>(),own_node,ignore_nodes);//call the recursive replace function
    auto_append_in_ptree(it.second,own_node,ignore_nodes);//go down one generation
  }
}

/**
  * @fn void SetRootVerbosity(boost::optional<int>&& rvl)
  * @param rvl: verbosity level read from config file
  * @brief Sets RooFit and Root message levels
  */
inline void SetRootVerbosity(boost::optional<int>&& rvl){
  if(!rvl) return;
  if(*rvl == -1)
    gErrorIgnoreLevel = kUnset;
  else if(*rvl == 0)
    gErrorIgnoreLevel = kPrint;
  else if(*rvl == 1)
    gErrorIgnoreLevel = kInfo;
  else if(*rvl == 2 || *rvl == 3)
    gErrorIgnoreLevel = kWarning;
  else if(*rvl == 4)
    gErrorIgnoreLevel = kError;
  else if(*rvl == 5)
    gErrorIgnoreLevel = kFatal;
  else
    gErrorIgnoreLevel = kBreak;
  //DEBUG=0, INFO=1, PROGRESS=2, WARNING=3, ERROR=4, FATAL=5
  RooMsgService::instance().setGlobalKillBelow(static_cast<RooFit::MsgLevel>(*rvl));
}

/**
  * @fn std::vector<std::string> parse_filenames(STR&& input_file_names, const std::string& workdir = ".",
  *                                              const MessageService& msgsvc = MessageService("IOjuggler:parse_filenames",MSG_LVL::INFO))
  * @param input_file_names: l- or r-value of filenames as semicolon separated list or in an ASCII file given by @<filename>
  * @param workdir: In case of an empty TChain, try to prepend workdir to the input_file_names
  * @param msgsvc: pass MessageService of calling function for debugging
  * @return vector with parsed file names
  * @exception Throws if the vector to be returned is empty
  * @brief Parses input files
  */
template <typename STR = std::string>//template because we want to use r- and l-value std::strings
std::vector<std::string> parse_filenames(STR&& input_file_names, const std::string& workdir = "",
                                         const MessageService& msgsvc = MessageService("IOjuggler:parse_filenames",MSG_LVL::INFO)){
  std::vector<std::string> input_files,tmp_inputs;
  boost::algorithm::split(tmp_inputs, input_file_names, boost::algorithm::is_any_of(";"));
  for(const auto& inf : tmp_inputs){
    //if the input string starts with '@', we have to parse a whole input file
    if(boost::algorithm::starts_with(inf,"@")){
      std::string line;
      std::ifstream myfile;
      myfile.open(inf.substr(1).data());//substr returns the string without the '@'
      if(!myfile){
        myfile.open((workdir + "/" + inf.substr(1)).data());
        if(!myfile){
          msgsvc.errormsg("Could not find "+ inf + ". Skipping to next input file (if any)");
          continue;
        }
      }
      while(std::getline (myfile,line))
        input_files.push_back(line);//TODO: could be that there is a trailing character that needs to be removed
      myfile.close();
    }
    else input_files.push_back(inf);
  }
  if(input_files.empty()) throw std::runtime_error("Could not parse "+input_file_names);
  return input_files;
}

/**
  * @fn std::unique_ptr<TChain> get_chain(STR1&& treename, STR2&& input_file_names,
                                  const std::string& workdir = ".", const MessageService& msgsvc = MessageService("RootGetter:get_chain",MSG_LVL::INFO),
                                  const std::string& chain_title = "Chain")
  * @param treename: location of TTree within the files that will be added to the TChain
  * @param input_file_names: semicolon separated list of input files or ASCII file given by @<filename> with list of input files
  * @param workdir: In case of an empty TChain, try to prepend workdir to the input_file_names
  * @param msgsvc: pass MessageService of calling function for debugging
  * @param chain_title: Title given to returned TChain
  * @return TChain with parsed inputs
  * @exception Throws if the TChain does not point to a TFile (i.e. the chain is empty). Can also throw if the input files cannot be parsed
  * @brief Parses input files and puts together a TChain
  * @details The TChain is assembled from a given treename and a list of input files that get parsed in this function.
  *  These can be given as semicolon-separated list which may contain wildcards.<br>
  *  Alternatively, the location of an ASCII file can be given. Similar to \c hadd, the filename is required to start with '@'<br>
  *  Internally, the filenames are parsed by the parse_filenames function.
  * @attention Don't mix input files that should be prepended with `workdir` with files at absolute paths. Give all of them with absolute paths instead
  */
template <typename STR1 = std::string, typename STR2 = std::string>//template because we want to use r- and l-value std::strings
std::unique_ptr<TChain> get_chain(STR1&& treename, STR2&& input_file_names,
                                  const std::string& workdir = "", const MessageService& msgsvc = MessageService("IOjuggler:get_chain",MSG_LVL::INFO),
                                  const std::string& chain_title = "Chain"){
  auto input_files = parse_filenames(input_file_names,workdir,msgsvc);
  if(msgsvc.GetMsgLvl() > MSG_LVL::INFO)
    for(const auto& f : input_files)
      msgsvc.debugmsg("Got file: "+f);
  std::unique_ptr<TChain> ch{new TChain(treename.data(),chain_title.data())};
  const auto tmp_ign_lvl = gErrorIgnoreLevel;
  gErrorIgnoreLevel = kFatal;//to ignore file not found messages from ROOT
  for(auto& infn : input_files)
    ch->Add(infn.data());//do without workdir first to be able to quickly add xrootd files without checking TFile::Open on each one of them
  if(!ch->GetFile()){//this is bad for the cornercase where you try to add files from different locations
    ch->Reset();//OK, we found nothing, but we still need to reset the chain. otherwise further errors will be thrown down the line
    msgsvc.debugmsg("Cannot find input files. Trying to prepend workdir.");
    for(auto& infn : input_files)
      ch->Add((workdir+"/"+infn).data());
    if(!ch->GetFile())
      throw std::runtime_error("TChain with tree name " + static_cast<std::string>(treename) +
         " is empty. Check inputs at " + static_cast<std::string>(input_file_names));
  }
  gErrorIgnoreLevel = tmp_ign_lvl;
  return std::move(ch);
}

/**
  * @fn split_at_last_occurence(const std::string& string_to_split, const std::string& delimiter = ":")
  * @return a two-element array of split strings
  * @brief Splits input string into two at the last occurence of delimiter
  * @details Use case: \c PFN:tree combinations like \c root://eoslhcb.cern.ch://eos/lhcb/wg/BandQ/EXOTICS/Lb2LcD0K/somefile.root:some_tree
  */
std::array<std::string,2> split_at_last_occurence(const std::string& string_to_split, const std::string& delimiter = ":"){
  const auto split_pos = string_to_split.find_last_of(delimiter);
  if(split_pos != std::string::npos)//was delimiter found at all?
    return {std::move(string_to_split.substr(0,split_pos)), std::move(string_to_split.substr(split_pos+1))};
  else throw std::runtime_error("substr1<delimiter>substr2 combination not found in "+string_to_split);
}

/**
  * @fn std::vector< TFriendElement*> get_friends(const pt::ptree& options, TREE& tree,
                                                  const MessageService& msgsvc = MessageService("IOjuggler:get_friends",MSG_LVL::INFO), const std::string& chain_title = "Chain")
  * @param options: property tree with command-line options
  * @param tree: TTree to which friend trees should be added
  * @param msgsvc : external or dummy MessageService
  * @param chain_title : Title of the Chain of friend trees
  * @return a vector of TChain,TFriendElement unique-pointers, so that they don't go out of scope
  * @brief Parses non-options of the property tree containing the command-line options and adds friend trees
  * @details The calling script must be set up similar to:
  *   \code
  *     auto options = IOjuggler::parse_options(argc, argv, "d:i:t:h","nonoptions: any number of <file(s):friendtree> combinations");
  *     auto wd = options.get<std::string>("workdir");
  *     auto input = IOjuggler::get_file(options.get<std::string>("infilename"),wd);
  *     auto tree = IOjuggler::get_obj<TTree>(input,options.get<std::string>("treename"));
  *     auto my_friends = IOjuggler::get_friends(options,tree);
  *   \endcode
  * Here, \c i and \c t options are used to get the initial tree and \c d for the "workdir" key. <br>
  * Friend trees are documented in: https://root.cern.ch/doc/master/classTTree.html#a011d362261b694ee7dd780bad21f030b <br>
  * The returned vector just keeps the friends alive in the calling scope, so that they can be accessed when the main tree is called <br>
  * The funtion is templated so that it also works with TChains, or other classes inheriting from TTree.
  * \todo Implement SFINAE: Problematic because we also want to pass std::unique_ptr<TTree> etc, and it's not striaght forward (possible)
  * to deduce the basic type of the unique_ptr. Maybe c++17's constexpr if can do something here
  */
template< typename TREE>
inline std::vector<TFriendElement*> get_friends(const pt::ptree& options, TREE& tree,
                                                const MessageService& msgsvc = MessageService("IOjuggler:get_friends",MSG_LVL::INFO),
                                                const std::string& chain_title = "Chain"){

  if(auto eas = options.get_child_optional("extraargs")){
    std::vector<TFriendElement*> fes;
    unsigned int aux = 0;
    //iterate non-options
    for(const auto& ea : *eas){
      try {
        ++aux;
        const auto& [filename, treename] = split_at_last_occurence(ea.second.data());
         //1st argument of AddFriend: TChain pointer with unique name, 2nd argument: name of friend chain
        fes.emplace_back(tree->AddFriend(get_chain(treename, filename, options.get<std::string>("workdir"), msgsvc,
                                         chain_title+std::to_string(aux)).release(), (chain_title+"_"+std::to_string(aux)).data()));
        msgsvc.infomsg("Added tree "+treename+" from file(s) "+filename+" as friend-tree(s)");
      }
      catch (std::exception& e){ msgsvc.warningmsg("Caught exception "+static_cast<std::string>(e.what())); }
    }
    //return the vector if something is there, fall back to nullptr otherwise
    if(fes.size() > 0) return fes;
  }
  return {nullptr};
}

/**
  * @fn void replace_from_cmdline(pt::ptree& conf, const std::string& wildcards, const MessageService& msgsvc = MessageService("IOjuggler:replace_from_cmdline",MSG_LVL::INFO))
  * @param conf: property tree containing configuration for calling executable
  * @param wildcards: string with search and replace commdans from command line. Should look like <search0:replace0;search1:replace1;...>
  * @param msgsvc : external or dummy MessageService
  * @brief Replaces strings in the config file at runtime
  * @details Allows to search and replace essentially everything in the config file at runtime.
             Useful for passing on wildcards created at higher level, allowing to template config files.
  */
void replace_from_cmdline(pt::ptree& conf, const std::string& wildcards, const MessageService& msgsvc = MessageService("IOjuggler:replace_from_cmdline",MSG_LVL::INFO)){
  if(wildcards.empty())return;
  std::vector<std::string> search_and_replace_pairs;
  boost::algorithm::split(search_and_replace_pairs, wildcards, boost::algorithm::is_any_of(";"));
  for(const auto& sarp : search_and_replace_pairs){
    const auto split_pos = sarp.find_first_of(':');
    if( split_pos != std::string::npos )
      replace_stuff_in_ptree(conf, sarp.substr(0,split_pos), sarp.substr(split_pos+1), std::string{""}, std::vector<std::string>{std::string{""}});
    else msgsvc.warningmsg("Could not parse "+sarp);
  }
}

/**
  * @fn TFile create_output_file(const std::string& outfilename, const std::string& workdir = "", const std::string& outfile_option = "recreate",
                                  const std::string& outfile_title = "", int& outfile_compression = ROOT::RCompressionSetting::EDefaults::kUseCompiledDefault
                                  const MessageService& msgsvc = MessageService("IOjuggler:create_output_file",MSG_LVL::INFO))
  * @param outfilename         : name of the returned TFile
  * @param workdir             : prefixes output file name with this string
  * @param outfile_option      : specifies the mode in which the file is opened
  * @param outfile_title       : the title of the file
  * @param outfile_compression : specifies the compression algorithm and level
  * @param msgsvc              : external or dummy MessageService
  * @brief wraps TFile ctor (https://root.cern.ch/doc/v622/classTFile.html#ad0377adf2f3d88da1a1f77256a140d60) to make sure the workdir, outfilename and options are set correctly
  */
inline TFile create_output_file(const std::string& outfilename, const std::string& workdir = "", const std::string& outfile_option = "recreate",
                                const std::string& outfile_title = "", const int& outfile_compression = ROOT::RCompressionSetting::EDefaults::kUseCompiledDefault,
                                const MessageService& msgsvc = MessageService("IOjuggler:create_output_file",MSG_LVL::INFO)){
  const auto oloc = workdir.empty()? outfilename : workdir+"/"+outfilename;
  dir_exists(oloc,msgsvc);
  return TFile(oloc.data(),outfile_option.data(),outfile_title.data(),outfile_compression);
}

}
/*! @} End of Doxygen Groups*/

#endif
