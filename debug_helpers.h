#ifndef DEBUG_HELPERS_H
#define DEBUG_HELPERS_H
#include <type_traits>
#include <typeinfo>
#ifndef _MSC_VER
#   include <cxxabi.h>
#endif
#include <cmath>
#include <iostream>
#include <ostream>
#include <iomanip>

#include "TString.h"
#include "MessageService.h"

/**
  * @file debug_helpers.h
  * @version 0.3
  * @author Marian Stahl
  * @date  2018-10-19
  * @brief Helper functions for debugging
  */

/*!
 *  @addtogroup IOjuggler
 *  @{
 */

namespace IOjuggler {

/**
  * @author http://stackoverflow.com/questions/81870/is-it-possible-to-print-a-variables-type-in-standard-c
  * @fn template <class T> std::string type_name()
  * @return type of template as string
  * @brief Get type name of T. Use like e.g. @code std::cout << "decltype(ci) is " << type_name<decltype(ci)>() << '\n'; @endcode
  * or @code std::cout << "decltype((ci)) is " << type_name<decltype((ci))>() << '\n'; @endcode
  */
template <class T> std::string type_name(){
  typedef typename std::remove_reference<T>::type TR;
  std::unique_ptr<char, void(*)(void*)> own
      (
      #ifndef _MSC_VER
        abi::__cxa_demangle(typeid(TR).name(), nullptr, nullptr, nullptr),
      #else
        nullptr,
      #endif
        std::free
        );
  std::string r = own != nullptr ? own.get() : typeid(TR).name();
  if (std::is_const<TR>::value)
    r += " const";
  if (std::is_volatile<TR>::value)
    r += " volatile";
  if (std::is_lvalue_reference<T>::value)
    r += "&";
  else if (std::is_rvalue_reference<T>::value)
    r += "&&";
  return r;
}

/**
  * @author someone on stackoverflow (slightly modified)
  * @fn loadbar(unsigned int x, unsigned int n, unsigned int w = 100)
  * @param x : current event in a loop
  * @param n : number of entries we want to loop
  * @param w : width of loadbar, updates @ every integer step
  * @deprecated please use the ProgressBar class now
  * @brief Progress-bar to use in loops. Use like
  * @code //before loop
  *       auto nentries            = tree->GetEntries();
  *       const double one_percent = std::floor(nentries/100);
  *       unsigned int percentage  = static_cast<unsigned int>(one_percent);
  *       //in loop
  *       for (decltype(nentries) evt = 0; evt < nentries; evt++){
  *       tree->GetEntry(evt);
  *       if(evt == percentage){
  *         percentage += one_percent;
  *         loadbar(static_cast<unsigned int>(evt),static_cast<unsigned int>(nentries));
  *       }
  * @endcode
  */
static inline void loadbar(unsigned int x, unsigned int n, unsigned int w = 100)
{
  float        ratio  =  x/(float)n;
  unsigned int c      =  ratio * w;
  std::cout << std::setw(3) << static_cast<int>(std::ceil(ratio*100)) << "% [";
  for (unsigned int x=0; x<=c; x++) std::cout << "=";
  for (unsigned int x=c+1; x<w; x++) std::cout << " ";
  std::cout << "]\r" << std::flush;
}


/**
  * @fn inline std::string formatTex_towords(std::string name)
  * @brief replace some substring in the "name" variable, to be correctly handled by Tex. digits are formatted into words (0 --> Zero, 1 --> One, ...)
  * @param name : string containing digits that can't be handled by TeX's \c \\def command
  */
inline std::string formatTex_towords(std::string name){

  //define vector for characters which get replaced
  std::vector<std::pair<std::string,std::string>> texrepl =
  {{"0","Zero"},{"1","One"},{"2","Two"},{"3","Three"},{"4","Four"},{"5","Five"},{"6","Six"},{"7","Seven"},{"8","Eight"},{"9","Nine"}};

  //replace the digits with the words
  for(const auto& r : texrepl)
    boost::algorithm::replace_all(name, r.first,r.second); //replace all digits by their names

  //remove the non alphabetical chars
  name.erase(std::remove_if(name.begin(), name.end(),[](const auto& c){return !std::isalpha(c);}), name.end());

  return name;
}

/**
  * @fn inline std::string formatTex_todigits(std::string name)
  * @brief replace some substring in the "name" variable. words are formatted into digits (Zero --> 0, One --> 1, ...)
  * @param name : string containing digits as words
  */
inline std::string formatTex_todigits(std::string name){

  //define vector for characters which get replaced
  std::vector<std::pair<std::string,std::string>> texrepl =
  {{"0","Zero"},{"1","One"},{"2","Two"},{"3","Three"},{"4","Four"},{"5","Five"},{"6","Six"},{"7","Seven"},{"8","Eight"},{"9","Nine"}};

  //replace the words with the digits
  for(const auto& r : texrepl)
    boost::algorithm::replace_all(name, r.second,r.first);

  //remove the non alpha-numerical chars
  name.erase(std::remove_if(name.begin(), name.end(),[](const auto& c){return !(std::isalpha(c) || std::isdigit(c));}), name.end());

  return name;
}


}
/*! @} End of Doxygen Groups*/
#endif
