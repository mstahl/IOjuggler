import os.path
from snakemake.io import Namedlist

def get_exe(exname):
  """
    Check if the script that we want to run is there and return it.
  """
  if os.path.isfile("build/bin/"+exname): return "build/bin/"+exname
  elif os.path.isfile("$NTGROOT/build/bin/"+exname): return "$NTGROOT/build/bin/"+exname
  else : raise Exception("Could not find "+exname+" executable in build/bin or $NTGROOT/build/bin")

def parse_samples(*samples):
  """
    Turn a mixture of lists and strings from various locations into a single string in which the inputs are separated by semicolons.
  """
  ifn = ""
  for sample in samples:
    if sample:
      if isinstance(sample,str): ifn += sample+";"
      elif isinstance(sample,list): ifn += ";".join(sample)+";"
      else : raise Exception("Cannot parse samples")
  return ifn[:-1]

def parse_joker(param_dict, known_keys=[]):
  """
    Turn params dict into a single string leaving out those keys that are known.
  """
  joker = ""
  for k,v, in param_dict.items():
    if k not in known_keys:
      if isinstance(v,str): joker += "@{}:{};".format(k,v)
      elif isinstance(v,dict) or isinstance(v,Namedlist):
        joker += ";".join(["@{}:{}".format(nk,nv) for nk,nv in v.items()])+";" # we need a trailing ";" for possibly follwing params
  if joker: return "-j '"+joker[:-1]+"'" # if we have joker arguments, we use -j '{stuff}' and remove the trailing ";"
  else: return ""

def parse_friends(tree_name, workflow_friends=None, non_workflow_friends=None):
  """
    Turn a mixture of lists and strings from various locations into a single string in which the inputs are separated by semicolons.
    Here, the distinction between friends from with the workflow (treename needs to be appended) and out of the workflow are made.
    We assume that it is always possible to append the treename when passing the friend to snakemake.params.
  """
  friends = "'"
  if workflow_friends:
    if isinstance(workflow_friends,str): friends += workflow_friends+":"+tree_name+"'  "
    elif isinstance(workflow_friends,list): friends += str(":"+tree_name+"' '").join(workflow_friends)+":"+tree_name+"' '"
    else : raise Exception("Cannot parse friends")
  if non_workflow_friends:
    if isinstance(non_workflow_friends,str) and ":" in non_workflow_friends:
      friends += non_workflow_friends+"' '"
    elif isinstance(non_workflow_friends,list): friends += "' '".join(non_workflow_friends) + "'  "
    else : raise Exception("Cannot parse friends")
  if friends != "'" : return friends[:-2]
  else: return ""
