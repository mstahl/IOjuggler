#ifndef ROOTGETTER_H
#define ROOTGETTER_H
//ROOT
#include "TROOT.h"
#include "TString.h"
#include "TSystem.h"
#include "TFile.h"
#include "TTree.h"
//ROOFIT
#include "RooWorkspace.h"
//STL
#if __has_include(<filesystem>)
  #include <filesystem>
  namespace fs = ::std::filesystem;
#elif __has_include(<experimental/filesystem>)
  #include <experimental/filesystem>
  namespace fs = ::std::experimental::filesystem;
#else
  #error "too old for filesystem..."
#endif
#include <exception>
#include <string>
//local
#include "MessageService.h"

/**
  * @file RootGetter.h
  * @version 0.3
  * @author Marian Stahl
  * @date  2018-05-16
  * @brief Get objects from ROOT with some safe-guards, check if directories exist on the file system
  */

/*!
 *  @addtogroup IOjuggler
 *  @{
 */

namespace IOjuggler {

/**
  * @fn TFile* get_file(STR1&& infilename, STR2&& workdir = std::string("."), const char* opt = "READ",
                       const MessageService& msgsvc = MessageService("RootGetter:get_file",MSG_LVL::ERROR),
                       EnableIfstringConvertible<STR1> = nullptr, EnableIfstringConvertible<STR2> = nullptr)
  * @param infilename: r- or l-value std::string containing (path/to/)filename
  * @param workdir: optional workdir, usually parsed above
  * @param msgsvc: external or dummy MessageService
  * @param opt: see https://root.cern.ch/doc/master/classTFile.html#aadd8e58e4d010c80b728bc909ac86760
  * @return pointer to requested file
  * @exception runtime exception if file not found
  */
template <typename STR1 = std::string, typename STR2 = std::string>//template because we want to use r- and l-value std::strings
inline TFile* get_file(STR1&& infilename, STR2&& workdir = std::string(""), const char* opt = "READ",
                       const MessageService& msgsvc = MessageService("RootGetter:get_file",MSG_LVL::INFO),
                       EnableIfstringConvertible<STR1> = nullptr, EnableIfstringConvertible<STR2> = nullptr){

  // try to locate the file prepended with workdir on the local fs
  std::string const location = workdir.empty() ? infilename : workdir+"/"+infilename;
  msgsvc.debugmsg("Got this input : "+location);
  if(auto fn = fs::path(location); fs::exists(fn)){
    msgsvc.debugmsg("Getting file from "+static_cast<std::string>(fn.c_str()));
    auto localfile = TFile::Open(fn.c_str() ,opt);
    if(localfile != nullptr && !localfile->IsZombie())
      return localfile;
  }
  // if the local file is not there, it could be that the file is at a remote site or the full path has been given
  // silence root for the moment, since you might want to catch the exception and handle the error yourself
  auto const eil = gErrorIgnoreLevel;
  gErrorIgnoreLevel = kFatal;
  auto infile = TFile::Open(infilename.data() ,opt);
  gErrorIgnoreLevel = eil;
  if(infile == nullptr || infile->IsZombie())
    throw std::runtime_error("get_file: cannot get file from "+infilename+" or "+location);
  return infile;
}

/**
  * @fn OBJTYPE* get_obj(TFile* infile, STR&& objname,
                        const MessageService& msgsvc = MessageService("RootGetter:get_obj",MSG_LVL::ERROR),
                        EnableIfstringConvertible<STR> = nullptr)
  * @param infile: pointer to TFile which contains the TObject
  * @param objname: name of the object
  * @param msgsvc: external or dummy MessageService
  * @brief Get stuff from a \c .root file
  * @details Use like e.g. @code auto tree = IOjuggler::get_obj<TTree>(infile,"mytree"); @endcode to get TTree* object from infile
  * @return pointer to requested object
  * @exception runtime exception if object not found in file
  */
template <class OBJTYPE, typename STR = std::string>
inline OBJTYPE* get_obj(TFile* infile, STR&& objname,
                        const MessageService& msgsvc = MessageService("RootGetter:get_obj",MSG_LVL::ERROR),
                        EnableIfstringConvertible<STR> = nullptr){
  OBJTYPE* obj = static_cast<OBJTYPE*>(infile->Get(static_cast<TString>(objname).Data()));
  if(obj == nullptr){
    msgsvc.errormsg(TString::Format("object %s not found in %s",static_cast<TString>(objname).Data(),infile->GetName()));
    throw std::runtime_error("get_obj: cannot get TObject from input file");
  }
  return obj;
}

/**
  * @fn OBJTYPE* get_wsobj(const RooWorkspace* w, STR&& objname,
                          const MessageService& msgsvc = MessageService("RootGetter:get_wsobj",MSG_LVL::ERROR),
                          EnableIfstringConvertible<STR> = nullptr)
  * @param w: pointer to RooWorkspace which contains the object
  * @param objname: name of the object
  * @param msgsvc: external or dummy MessageService
  * @brief Get stuff from a \c RooWorkspace
  * @details Use like e.g. @code auto pdf = IOjuggler::get_wsobj<RooAbsPdf>(w,"mypdf"); @endcode to get RooAbsPdf* object from workspace
  * @return pointer to requested object
  * @exception runtime exception if object not found in workspace
  */
template <class OBJTYPE, typename STR = std::string>
inline OBJTYPE* get_wsobj(const RooWorkspace* w, STR&& objname,
                          const MessageService& msgsvc = MessageService("RootGetter:get_wsobj",MSG_LVL::ERROR),
                          EnableIfstringConvertible<STR> = nullptr){
  OBJTYPE* obj = static_cast<OBJTYPE*>(w->obj(static_cast<TString>(objname).Data()));
  if(obj == nullptr) {
    msgsvc.errormsg(TString::Format("object %s not found in RooWorkspace %s",static_cast<TString>(objname).Data(),w->GetName()));
    throw std::runtime_error("get_obj: cannot get TObject from input workspace");
  }
  return obj;
}
}
/*! @} End of Doxygen Groups*/
#endif
