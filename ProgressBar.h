#ifndef PROGRESSBAR_H
#define PROGRESSBAR_H
#include <iostream>
#include <ostream>
#include <iomanip>
#include <chrono>
#include <thread>
#include <stdexcept>
#include "MessageService.h"
#ifdef USE_OPENMP
//openMP
#include <omp.h>
#else
#define omp_get_num_threads() 1
#endif

/**
  * @file ProgressBar.h
  * @version 0.2
  * @author someone on stackoverflow (slightly modified) see https://stackoverflow.com/questions/28050669/can-i-report-progress-for-openmp-tasks
  * @date  2018-05-16
  * @brief Progress-bar to use in (OpenMP) loops
  */


/**
  * @class Timer
  * @brief Helper class for ProgressBar that does the time measurements
  * @details Measure times such as how long it takes a given function to run, or how long I/O has taken.
  */
class Timer{
private:
  typedef std::chrono::high_resolution_clock clock;
  typedef std::chrono::duration<double, std::ratio<1> > second;

  std::chrono::time_point<clock> start_time; ///< Last time the timer was started
  double accumulated_time;                   ///< Accumulated running time since creation
  bool running;                              ///< True when the timer is running

public:
  Timer(){
    accumulated_time = 0;
    running          = false;
  }

  ///Start the timer. Throws an exception if timer was already running.
  void start(){
    if(running)
      throw std::runtime_error("Timer was already started!");
    running=true;
    start_time = clock::now();
  }

  ///Stop the timer. Throws an exception if timer was already stopped.
  ///Calling this adds to the timer's accumulated time.
  ///@return The accumulated time in seconds.
  double stop(){
    if(!running)
      throw std::runtime_error("Timer was already stopped!");

    accumulated_time += lap();
    running           = false;

    return accumulated_time;
  }

  ///Returns the timer's accumulated time. Throws an exception if the timer is
  ///running.
  double accumulated(){
    if(running)
      throw std::runtime_error("Timer is still running!");
    return accumulated_time;
  }

  ///Returns the time between when the timer was started and the current
  ///moment. Throws an exception if the timer is not running.
  double lap(){
    if(!running)
      throw std::runtime_error("Timer was not started!");
    return std::chrono::duration_cast<second> (clock::now() - start_time).count();
  }

  ///Stops the timer and resets its accumulated time. No exceptions are thrown
  ///ever.
  void reset(){
    accumulated_time = 0;
    running          = false;
  }
};



/**
  * @class ProgressBar
  * @brief Manages a console-based progress bar to keep the user entertained.
  * @details The progress bar also works with multithreading. Use like this:
  * \code
  *   unsigned int nentries = 1000;//or tree->GetEntries() etc.
  *   ProgressBar pg;
  *   pg.start(nentries,msgsvc);
  *   for(unsigned int evt = 0; evt < nentries; evt++){
  *     //do stuff in the loop here
  *     pg.update(evt);
  *     //or here
  *   }
  *   const int time = pg.stop();
  *   msgsvc.infomsg("Elapsed time for filling dataset: "+std::to_string(time)+" s");
  * \endcode
  */
class ProgressBar{
private:
  //! Total work to be accomplished
  unsigned int total_work;
  //! Next point to update the visible progress bar
  unsigned int next_update;
  //! Interval between updates in work units
  unsigned int call_diff;
  //! Work already done
  unsigned int work_done;
  //! Old percentage value (aka: should we update the progress bar) TODO: Maybe that we do not need this
  unsigned int old_percent;
  //! Used for generating ETA
  Timer    timer;
  //! Number of threads
  int nthreads;
  //! MessageService for printing to stdout
  MessageService msgsvc;

  ///Clear current line on console so a new progress bar can be written
  void clearConsoleLine() const {
    std::cerr<<"\r\033[2K"<<std::flush;
  }

public:
  /**
    * @fn start(unsigned int total_work, MessageService& mess)
    * @brief start timer and bar
    * @param total_work: size of loop
    * @param mess: Highest message level that should be printed to stdout
    */
  void start(unsigned int total_work, MessageService& mess, const int& threads = 1){
    timer = Timer();
    timer.start();
    this->total_work = total_work;
    next_update      = 0;
    call_diff        = total_work/200;
    old_percent      = 0;
    work_done        = 0;
    msgsvc           = mess;
    nthreads = omp_get_num_threads() == 1 ? threads : omp_get_num_threads();
    clearConsoleLine();
  }

  /**
    * @fn update(unsigned int work_done0)
    * @brief update progress bar in loop
    * @param work_done0: variable that gets incremented in the loop
    */
  void update(unsigned int work_done0){

    //Update the amount of work done
    work_done = work_done0;

    //Quick return if insufficient progress has occurred
    if(work_done<next_update)
      return;

    //Update the next time at which we'll do the expensive update stuff
    next_update += call_diff;

    unsigned int percent = work_done*100/total_work;

    //Handle overflows
    if(percent>100)
      percent=100;

    //In the case that there has been no update (which should never be the case,
    //actually), skip the expensive screen print
    if(percent==old_percent)
      return;

    //Update old_percent accordingly
    old_percent=percent;

    //Print an update string which looks like this:
    //  [================================================  ] (96% - ETA: 1.0s - 4 threads)
    msgsvc.print_progress(percent,timer.lap()/percent*(100-percent),nthreads);

  }

  ///Increment by one the work done and update the progress bar
  ProgressBar& operator++(){
    work_done++;
    update(work_done);
    return *this;
  }

  /**
    * @fn stop()
    * @brief Stop the timer and return the time
    * @return consumed time in seconds
    */
  double stop(){
    clearConsoleLine();

    timer.stop();
    return timer.accumulated();
  }

  ///@return Return the time the progress bar ran for.
  double time_it_took(){
    return timer.accumulated();
  }

  unsigned int cellsProcessed() const {
    return work_done;
  }
};



#endif
